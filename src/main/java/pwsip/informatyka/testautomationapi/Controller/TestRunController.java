package pwsip.informatyka.testautomationapi.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import pwsip.informatyka.testautomationapi.Entity.TestRunEntity;
import pwsip.informatyka.testautomationapi.Service.TestRunService;

@RestController
@RequestMapping("/run")
//@CrossOrigin(origins = "${frontendApp.mainURL}", maxAge = 3600)
public class TestRunController {

    @Autowired
    TestRunService testRunService;

    private Logger logger = LoggerFactory.getLogger(TestRunController.class);

    @RequestMapping(value="/newTest", method = RequestMethod.POST)
    public ResponseEntity<String> saveNewTest(@RequestBody TestRunEntity testRunEntity){
        testRunService.saveTestRun(testRunEntity);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @RequestMapping("/start")
    @Scheduled(cron = "0 */10 * * * *")
    public void start(){
        if(testRunService.getNumberOfOpenTestRuns() > 0) {
            testRunService.runTest();
            logger.info("Test succesfully started");
        }
        else {
            logger.info("No test to execute");
        }
    }
}
