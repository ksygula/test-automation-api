package pwsip.informatyka.testautomationapi.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pwsip.informatyka.testautomationapi.Entity.MethodEntity;
import pwsip.informatyka.testautomationapi.Service.MethodsService;
import pwsip.informatyka.testautomationapi.Service.TestClassOperationsService;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/methods")
@CrossOrigin(origins = "${frontendApp.mainURL}", maxAge = 3600)
public class TestMethodsController {

    @Autowired
    MethodsService methodsService;
    @Autowired
    TestClassOperationsService testClassOperationsService;
    @Value("${testNGtests.packageName}")
    String packageName;

    @RequestMapping("/getall")
    public List<MethodEntity> getMethods(){
        return methodsService.getMethodsList();
    }

    @RequestMapping("/gettestclass")
    public HashMap<String,String> getClasses() {
        HashMap<String, String> classes = new HashMap<String, String>();
        Set<Class<?>> set =  testClassOperationsService.getClassSetFromPackage(packageName);
        for(Class c : set){
            Method[] m = c.getDeclaredMethods();

            for(Method method : m){
                classes.putIfAbsent(method.getName(), c.getName());
            }
        }
        return classes;
    }
}
