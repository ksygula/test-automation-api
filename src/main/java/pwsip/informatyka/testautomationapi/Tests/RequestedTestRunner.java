package pwsip.informatyka.testautomationapi.Tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pwsip.informatyka.testautomationapi.Entity.SelectedTestMethodsEntity;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import java.util.Comparator;
import java.util.List;

public class RequestedTestRunner {

    private WebDriver driver;

    @BeforeMethod
    public void setUp(){
        System.setProperty("webdriver.gecko.driver", "C:\\java-apps\\test-automation-api\\utils\\geckodriver.exe");
        driver = new FirefoxDriver();
    }

    @Test(dataProvider = "TestData", dataProviderClass = TestOne.class)
    public void testSelectedMethods(List<SelectedTestMethodsEntity> testMethodsList) throws Exception {
        testMethodsList.sort(Comparator.comparing(SelectedTestMethodsEntity::getOrder));
        for(SelectedTestMethodsEntity testMethod: testMethodsList) {
            Class<?> c = Class.forName(testMethod.getClassName());
            Constructor<?> constructor = c.getConstructor(WebDriver.class);
            Object o = constructor.newInstance(driver);

            Method method = o.getClass().getMethod(testMethod.getMethodName());
            method.invoke(o);
        }
    }
}
