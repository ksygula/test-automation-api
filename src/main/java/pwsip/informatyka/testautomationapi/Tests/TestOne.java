package pwsip.informatyka.testautomationapi.Tests;

import org.testng.annotations.DataProvider;
import pwsip.informatyka.testautomationapi.Configuration.ApplicationContextHolder;
import pwsip.informatyka.testautomationapi.Service.TestRunService;

public class TestOne {

    @DataProvider(name = "TestData")
    public Object[][] getTestData() {
        TestRunService testRunService = ApplicationContextHolder.getContext().getBean(TestRunService.class);
        return new Object[][] {
                {
                        testRunService.prepareListOfSelectedTestMethods(testRunService.getFirstOpenTestRunAndChangeStatusToStarted())
                }
        };
    }
}
