package pwsip.informatyka.testautomationapi.Tests.PageObject;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class GoogleMainPage {
    public GoogleMainPage(WebDriver driver){
        this.driver = driver;
    }

    WebDriver driver;

    public void one(){
        driver.get("http://www.google.com");
    }

    public void two() {
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.google.com/?gws_rd=ssl");
    }
}
