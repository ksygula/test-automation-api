package pwsip.informatyka.testautomationapi.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import pwsip.informatyka.testautomationapi.Entity.Enum.TestStatus;

import javax.persistence.*;
import java.time.LocalDateTime;

import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "test_runs")
public class TestRunEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;
    private String user;
    @Column(name = "add_date")
    private LocalDateTime addDate;
    @Column(name = "end_date")
    private LocalDateTime endDate;

    @OneToMany(mappedBy = "testRunEntity", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<SelectedTestMethodsEntity> selectedTestMethodsEntities;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private TestStatus testStatus;

    @PrePersist
    void setMissingValues() {
        if(getAddDate() == null) {
            setAddDate(LocalDateTime.now());
        }
        if(getTestStatus() == null) {
            setTestStatus(TestStatus.OPEN);
        }
    }
}
