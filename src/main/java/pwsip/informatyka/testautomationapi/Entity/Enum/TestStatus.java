package pwsip.informatyka.testautomationapi.Entity.Enum;

public enum TestStatus {
    OPEN, STARTED, ABORTED, SKIPPED, PASSED, FAILED;
}
