package pwsip.informatyka.testautomationapi.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "selected_test_methods")
@AllArgsConstructor
@NoArgsConstructor
public class SelectedTestMethodsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;
    private String className;
    private String methodName;
    private Integer order;
    @ManyToOne
    @JoinColumn
    @JsonBackReference
    private TestRunEntity testRunEntity;

    @PrePersist
    void setOrder() {
        if (this.order == null) {
            this.order = this.id;
        }
    }
}
