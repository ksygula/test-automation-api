package pwsip.informatyka.testautomationapi.Repository;

import org.springframework.data.repository.CrudRepository;
import pwsip.informatyka.testautomationapi.Entity.MethodEntity;

public interface MethodRepository extends CrudRepository<MethodEntity, Integer> {
}
