package pwsip.informatyka.testautomationapi.Repository;

import org.springframework.data.repository.CrudRepository;
import pwsip.informatyka.testautomationapi.Entity.Enum.TestStatus;
import pwsip.informatyka.testautomationapi.Entity.TestRunEntity;

import java.util.List;

public interface TestRunRepository extends CrudRepository<TestRunEntity, Integer> {

    TestRunEntity findFirstByTestStatus(TestStatus testStatus);
    List<TestRunEntity> findAllByTestStatus(TestStatus testStatus);
}
