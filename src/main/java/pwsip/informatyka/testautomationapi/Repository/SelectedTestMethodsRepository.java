package pwsip.informatyka.testautomationapi.Repository;

import org.springframework.data.repository.CrudRepository;
import pwsip.informatyka.testautomationapi.Entity.SelectedTestMethodsEntity;

public interface SelectedTestMethodsRepository extends CrudRepository<SelectedTestMethodsEntity, Integer> {
}
