package pwsip.informatyka.testautomationapi.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pwsip.informatyka.testautomationapi.Entity.MethodEntity;
import pwsip.informatyka.testautomationapi.Repository.MethodRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class MethodsService {

    @Autowired
    MethodRepository methodRepository;

    public List<MethodEntity> getMethodsList() {
        List<MethodEntity> methodList =  new ArrayList<>();
        methodRepository.findAll().iterator().forEachRemaining(methodList::add);
        return methodList;
    }
}
