package pwsip.informatyka.testautomationapi.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.testng.TestNG;
import pwsip.informatyka.testautomationapi.Entity.Enum.TestStatus;
import pwsip.informatyka.testautomationapi.Entity.SelectedTestMethodsEntity;
import pwsip.informatyka.testautomationapi.Entity.TestRunEntity;
import pwsip.informatyka.testautomationapi.Repository.TestRunRepository;
import pwsip.informatyka.testautomationapi.Tests.RequestedTestRunner;

import java.util.ArrayList;
import java.util.List;

@Service
public class TestRunService {

    @Autowired
    TestRunRepository testRunRepository;

    public TestRunEntity saveTestRun(TestRunEntity testRunEntity){
        return testRunRepository.save(testRunEntity);
    }

    public void runTest(){
        TestNG testNG = new TestNG();
        testNG.setTestClasses(new Class[] {RequestedTestRunner.class});
        testNG.run();
    }

    public Integer getNumberOfOpenTestRuns() {
        return testRunRepository.findAllByTestStatus(TestStatus.OPEN).size();
    }

    public TestRunEntity getFirstOpenTestRunAndChangeStatusToStarted() {
        TestRunEntity testRunEntity = testRunRepository.findFirstByTestStatus(TestStatus.OPEN);
        changeTestStatusToStarted(testRunEntity);
        return testRunEntity;
    }

    public List<SelectedTestMethodsEntity> prepareListOfSelectedTestMethods(TestRunEntity testRunEntity) {
        return new ArrayList<>(testRunEntity.getSelectedTestMethodsEntities());
    }

    public void changeTestStatusToStarted(TestRunEntity testRunEntity) {
        testRunEntity.setTestStatus(TestStatus.STARTED);
        testRunRepository.save(testRunEntity);
    }
}
