package pwsip.informatyka.testautomationapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import pwsip.informatyka.testautomationapi.Tests.RequestedTestRunner;
import pwsip.informatyka.testautomationapi.Tests.TestOne;

@SpringBootApplication
@EnableScheduling
public class TestAutomationApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestAutomationApiApplication.class, args);
    }

}
